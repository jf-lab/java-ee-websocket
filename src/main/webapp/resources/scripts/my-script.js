let ws = new WebSocket("ws://localhost:8080/websockets-ui-interaction-1.0-SNAPSHOT/my-event-channel");

ws.onopen = ev => {
    console.log("websocket is opened");
}

ws.onmessage = ev => {
    console.log(`data has been received: ${ev.data}`);
}