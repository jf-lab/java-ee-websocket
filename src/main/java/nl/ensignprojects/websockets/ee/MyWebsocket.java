package nl.ensignprojects.websockets.ee;

import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

@ServerEndpoint(value = "/my-event-channel")
public class MyWebsocket implements Serializable {

    private static Set<Session> sessions = new HashSet<>();

    @OnOpen
    public void onOpen(Session session) throws IOException {
        sessions.add(session);


        System.out.println("We are starting to send messages...");
        TimerTask task = new TimerTask() {
            Integer messageCounter = 0;

            @Override
            public void run() {
                session.getAsyncRemote().sendText("My message: " + messageCounter++);
                System.out.println("Sessions open: " + sessions.size());
            }
        };
        new Timer("my").schedule(task, 0L, 1000L);
    }

    @OnClose
    public void onClose(Session session) {
        sessions.remove(session);
    }

}
