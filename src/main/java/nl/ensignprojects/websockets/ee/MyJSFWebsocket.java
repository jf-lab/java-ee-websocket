package nl.ensignprojects.websockets.ee;

import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;

@ViewScoped
@Named("helloBean")
public class MyJSFWebsocket implements Serializable {

    @Inject
    @Push
    private PushContext helloChannel;

    private String message;

    public void sendMessage() {
        System.out.println("Send push message");
        this.sendPushMessage("hello");
    }

    private void sendPushMessage(String message) {
        helloChannel.send("" + message + " at " + LocalDateTime.now());
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void sendMessage2() {
        // log.log(Level.INFO, "send push message from input box::" + this.message);
        this.sendPushMessage(this.message);
    }


}
